use std::net::SocketAddr;
use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
pub(crate) use simple_aries_agent::agent::Agent;
pub(crate) use simple_aries_agent::config::Config;
extern crate pretty_env_logger;
// use log4rs;
use clap;
use clap::{Arg, App as ShellApp};

#[macro_use] extern crate log;
#[actix_web::main]
async fn main() {

    pretty_env_logger::init();
    // log4rs::init_file("/var/lib/simple-aries-agent/log4rs.yaml", Default::default()).unwrap();

    let args = ShellApp::new("Simple Aries Agent")
        .version("1.0")
        .author("Matthew Spence <MatthewswSpence@gmail.com>")
        .about("Runs a simple aries wallet (or for now just a web server)")
        .arg(Arg::new("port")
            .short('p')
            .long("port")
            .value_name("PORT")
            .help("Sets a custom port number")
            .default_value("8080")
            .takes_value(true)).get_matches();

    let agent_port = args.value_of("port").unwrap_or("8080").parse::<u16>().unwrap_or(8080);

    info!("Creating new Agent");
    let agent = Agent::init(Config {
        walletName: "wallet1".to_string(),
        walletPassphrase: "defaultPassword".to_string()
    }).await;

    let mut attempts = 0u32;
    let socket_addr = SocketAddr::from(([127, 0, 0, 1], agent_port));
    let server = loop {
        attempts += 1;
        match HttpServer::new( || {
            App::new()
                .service(hello)
        }).bind(socket_addr) {
            Ok(http_server) => break http_server,
            Err(error) if attempts == 5 => {
                error!("Attempt {:?} failed with error {:?}", attempts, error);
                error!("5 failed attempts, aborting launch");
                std::process::exit(1)
            },
            Err(error) => {
                error!("Attempt {:?} failed with error {:?}", attempts, error);
                continue
            },
        }
    };
    server.run().await.unwrap();
    info!("HTTP Server Running on port {:?}", agent_port);
}

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello World!")
}

#[post("/message")]
async fn receive_message() -> impl Responder {
    HttpResponse::Ok().body("Message Received!")
}
