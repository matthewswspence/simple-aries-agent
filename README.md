# simple aries agent
## Description
A basic aries agent I'm building as a side project. The project contains 
two parts: a lib containing the agent object and a binary executable 
that wraps the lib with a webserver. The binary is managed via systemd 
and installed via a debian package. 

## Installation
In the project root run
`
    cargo deb && sudo dpkg -i /target/debian/simple-aries-agent_0.0.1_amd64.deb
`

## Usage
Build the project by running `cargo deb` in the root directory. In order to build you will need to clone my 
personal fork of vdr-tools [here](https://gitlab.com/matthewswspence/vdr-tools) and change the path for the 
libvdrtools cargo dependency to the path to `libvdrtools` crate on you system. which exposes the controller
layer for use in the agent. After that a web server should spin up that currently does approximately nothing. 
Default port is 8080. 

## Roadmap
### 0.1.0
Setup configuration, wallet storage, and a connection protocol (did exchange, OoB, Connection)

## Contributing
Open to any and all contributions. Just submit an MR and I'll give it a look. 

## License
Apache 2.0 License

## Project status
Under construction. 
