use std::borrow::Borrow;
use vdrtools::Locator;
use indy_api_types::domain::wallet::{Config, KeyDerivationMethod};
use indy_api_types::domain::wallet::Credentials;
use indy_api_types::errors::IndyResult;
use indy_api_types::WalletHandle;
use log::debug;

use crate::crypto;

pub async fn create_wallet(id: String, passphrase: String) -> WalletHandle {
    let locator = Locator::instance();

    let config  = Config {
        id,
        storage_type: None,
        storage_config: None,
        cache: None
    };

    let credentials = Credentials {
        key: passphrase,
        rekey: None,
        storage_credentials: None,
        key_derivation_method: KeyDerivationMethod::ARGON2I_MOD,
        rekey_derivation_method: KeyDerivationMethod::ARGON2I_MOD
    };

    debug!(
        "indy_create_wallet ? config {:?} credentials {:?}",
        config,
        credentials
    );

    locator.wallet_controller.create(config.clone(), credentials.clone()).await.unwrap();

    locator.wallet_controller.open(config.clone(), credentials.clone()).await.unwrap()
}

pub async fn open_wallet(id: String, passphrase: String) -> WalletHandle {
    let locator = Locator::instance();

    let config  = Config {
        id,
        storage_type: None,
        storage_config: None,
        cache: None
    };

    let credentials = Credentials {
        key: passphrase,
        rekey: None,
        storage_credentials: None,
        key_derivation_method: KeyDerivationMethod::ARGON2I_MOD,
        rekey_derivation_method: KeyDerivationMethod::ARGON2I_MOD
    };

    debug!(
        "indy_create_wallet ? config {:?} credentials {:?}",
        config,
        credentials
    );

    locator.wallet_controller.open(config.clone(), credentials.clone()).await.unwrap()
}

pub async fn check_wallet(name: String, passphrase: String) -> bool {
    let config  = Config {
        id: name,
        storage_type: None,
        storage_config: None,
        cache: None
    };

    let credentials = Credentials {
        key: passphrase,
        rekey: None,
        storage_credentials: None,
        key_derivation_method: KeyDerivationMethod::ARGON2I_MOD,
        rekey_derivation_method: KeyDerivationMethod::ARGON2I_MOD
    };

    let locator = Locator::instance();

    match locator.wallet_controller.open(config, credentials).await {
        Ok(walletHandle) => true,
        Err(error) => false
    }
}
