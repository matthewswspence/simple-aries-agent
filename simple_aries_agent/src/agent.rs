use std::borrow::Borrow;
use crate::config::Config;
use indy_api_types::WalletHandle;
use crate::wallet;

pub struct Agent {
    config: Config,
    wallet_handle: WalletHandle,
}

impl Agent {
    pub async fn from_config(config:Config) -> Agent{
        let key = config.walletPassphrase.clone();
        let id = config.walletName.clone();
        Agent {
            config,
            wallet_handle: wallet::open_wallet(id, key).await,
        }
    }

    pub async fn new() -> Agent {
        Agent {
            config: Config {
                walletName: "simple-aries-agent".to_string(),
                walletPassphrase: "default password".to_string(),
            },
            wallet_handle: wallet::create_wallet("simple-aries-agent".to_string(), "default password".to_string()).await,
        }
    }

    pub async fn init(config: Config) -> Agent {
        if wallet::check_wallet(config.walletName.clone(), config.walletPassphrase.clone()).await {
            return Agent::from_config(config).await
        }
        Agent::new().await
    }
}